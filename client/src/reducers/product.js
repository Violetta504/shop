
const initState = {
    products:[],
    totalSum:0,
    totalQuantity:0,
    messageSuccess:false
}

export default function product(state = {}, action)
{

    switch(action.type)
    {
        case "SET_PRODUCT": 
        
        return {
            ...state,
            ...action.data
        }

    }
    return state;
}