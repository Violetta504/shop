
const initState = {
    products:[],
    totalSum:0,
    totalQuantity:0,
    messageSuccess:false
}

export default function cart(state = initState, action)
{

    let upProducts = (type) =>
    {
        let up = false;

        let dataProducts = state.products.map((product, key)=>
        {
            if(action.data._id === product._id && (product.quantity > 1 || type))
            {
              
                up = true;

                let p = {...product}

                p.sum = type ? product.sum + product.price : product.sum - product.price;
            
                p.quantity = type ? product.quantity + 1 : product.quantity - 1;

                return p;
               
            }
            return product
        })

        return !up ? false : dataProducts;
    }
    

    const addProduct = () => {
        let data  = upProducts(true);
        return data != false ? data : [...state.products, {...action.data, quantity:1, sum:action.data.price}];
    }

    const removeProduct = () => {
        let data  = upProducts(false);
       
        return data != false ? data :state.products.filter((product)=>{
            if(product._id != action.data._id) return true;
        });
    }

    const getQuantity = () =>
    {
        let q = state.totalQuantity -1;
        console.log();
        if(q === 0) action.data.close();
        return q;
    }


    switch(action.type)
    {
        case "ADD_PRODUCT": 
        
        return {
            ...state,
            products:addProduct(),

            totalQuantity:state.totalQuantity +1,
            totalSum: state.totalSum + action.data.price
        }

        case "REMOVE_PRODUCT": 
        
        return {
            ...state,
            products:removeProduct(),
            totalQuantity:getQuantity(),
            totalSum: state.totalSum - action.data.price
        }

        case "CLEAR_CART": 
        
        return {
            ...state,
            products:[],
            totalQuantity:0,
            totalSum: 0,
            messageSuccess:false
        }

        case "SHOW_MESSAGE_SUCCESS": 
        
        return {
            ...state,
            messageSuccess:true
        }
    }
    return state;
}