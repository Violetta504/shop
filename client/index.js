import React from "react";
import ReactDOM from 'react-dom';

/*Main component for use store*/
import { Provider }     from 'react-redux';

/*function for created store*/
import { createStore }  from 'redux';

/*store objects*/
import reducer from './src/reducers';

// Creating store with devtools
const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

import App from "./src/App";

import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
    <Provider store = {store} >
        <BrowserRouter>
            <App /> 
        </BrowserRouter> 
    </Provider> , document.getElementById("app")
);